<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAuth()
    {

        // $user = factory('App\Models\User')->create();

        // $this->actingAs($user)
        //      ->get('/user');

        $this->json('POST', '/user', ['email' => 'Sally'])
             ->seeJson([
                'created' => true,
             ]);

        $response = $this->get('/user');

        $this->assertEquals(200, $response->status());

        // $this->get('/user');

        // $this->assertEquals(
        //     $this->app->version(), $this->response->getContent()
        // );
    }
}
