<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentSubjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_subjects', function (Blueprint $table) {
            $table->uuid('uuid');

            $table->string('subject_id');
            $table->datetime('enrolled_at');

            $table->string('status'); // enrolled, dropped, cancelled
            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('subject_id')
                ->references('uuid')->on('subjects')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('student_subjects')) {
            Schema::table('student_subjects', function (Blueprint $table) {
                $table->dropForeign('student_subjects_subject_id_foreign');
            });
        }

        Schema::dropIfExists('student_subjects');
    }
}
