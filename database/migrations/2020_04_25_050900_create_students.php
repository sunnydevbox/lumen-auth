<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');
            $table->string('password')->nullable();

            $table->string('id')->unique();

            $table->string('status'); // pending / enrolled / suspended / dropped

            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name')->nullable();

            $table->tinyInteger('gender');
            $table->date('dob');
            $table->string('birth_place')->nullable();
            $table->string('religion')->nullable();
            $table->string('nationality')->nullable();

            /** Primary Contact */
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->string('landline')->nullable();
            $table->string('fax')->nullable();

            /** Primary Address */
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('zipcode');
            $table->string('country');

            $table->index('id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
