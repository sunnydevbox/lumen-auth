<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id()->unsigned();

            $table->string('label');
            $table->text('description')->nullable();
            $table->boolean('status')->default(false); // 1 - active; 0 - inactive

            // $table->unsignedInteger('_lft');
            // $table->unsignedInteger('_rgt');

            $table->nestedSet();

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('lesson_categories', function($table) {
            $table->string('lesson_id');
            $table->index('lesson_id');

            $table->foreign('lesson_id')
                ->references('uuid')
                ->on('lessons');

            $table->bigInteger('category_id')->nullable()->unsigned();
            $table->index('category_id');

            $table->foreign('category_id')
                ->references('id')
                ->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson_categories', function($table) {
            $table->dropForeign('lesson_categories_category_id_foreign');
            $table->dropIndex('lesson_categories_category_id_index');

            $table->dropForeign('lesson_categories_lesson_id_foreign');
            $table->dropIndex('lesson_categories_lesson_id_index');

            // $table->dropColumn(['category_id', 'lesson_id']);
        });

        Schema::dropIfExists('lesson_categories');

        Schema::table('categories', function($table) {
            $table->dropNestedSet();
        });
        
        Schema::dropIfExists('categories');
    }
}
