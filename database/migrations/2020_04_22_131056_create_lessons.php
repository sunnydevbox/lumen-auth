<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->primary('uuid');
            $table->string('code')->unique()->nullable();

            $table->string('subject_id')->nullable();

            $table->string('name');
            $table->text('description');

            $table->float('view_count')->default(0);
            $table->float('rating')->default(0);


            // JWPLAYER 
            $table->string('video_status')->nullable(); // uploading, ready, ok, etc
            $table->float('video_duration')->default(0); // In seconds
            $table->string('video_key')->nullable();
            $table->string('video_url')->nullable();
            $table->integer('video_size')->default(0);
            $table->text('video_embed')->nullable();
            $table->datetime('video_created')->nullable();
            $table->datetime('video_updated')->nullable();

            $table->string('video_player_code')->nullable()->default('https://cdn.jwplayer.com/libraries/CgAci0sz.js');


            $table->string('status')->default('inactive');

            $table->foreign('subject_id')
                ->references('uuid')->on('subjects');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lessons', function (Blueprint $table) {
            $table->dropForeign('lessons_subject_id_foreign');
        });

        Schema::dropIfExists('lessons');
    }
}
