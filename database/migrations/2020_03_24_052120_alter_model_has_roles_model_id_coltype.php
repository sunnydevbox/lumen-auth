<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterModelHasRolesModelIdColtype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $columnNames = config('permission.column_names');

        Schema::table('model_has_roles', function($table) use ($columnNames) {
            $table->dropColumn([$columnNames['model_morph_key']]);
        });

        Schema::table('model_has_roles', function($table) use ($columnNames) {
            $table->string($columnNames['model_morph_key']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $columnNames = config('permission.column_names');
        //bigInteger
        Schema::table('model_has_roles', function($table) use ($columnNames) {
            $table->dropColumn([$columnNames['model_morph_key']]);
        });

        Schema::table('model_has_roles', function($table) use ($columnNames) {
            $table->bigInteger($columnNames['model_morph_key'])->unsigned();
        });
    }
}
