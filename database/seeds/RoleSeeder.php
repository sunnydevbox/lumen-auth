<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super = Role::create(['name' => 'super']); // System/Dev roles
        $faculty = Role::create(['name' => 'faculty', 'guard_name' => 'faculty']);
        $student = Role::create(['name' => 'student', 'guard_name' => 'student']);
    }
}
