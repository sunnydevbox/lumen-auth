<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super = Role::where(['name' => 'super'])->first(); // System/Dev roles
        $faculty = Role::where(['name' => 'faculty', 'guard_name' => 'faculty'])->first();
        $student = Role::where(['name' => 'student', 'guard_name' => 'student'])->first();
        // $teacher = Role::where(['name' => 'teacher', 'guard_name' => 'teacher'])->first();
        // $instructor = Role::where(['name' => 'instructor', 'guard_name' => 'instructor'])->first();

        /** ADMIN */
        // $student->givePermissionTo(Permission::create(['name' => 'browse courses', 'guard_name' => 'student']));
        // $student->givePermissionTo(Permission::create(['name' => 'read courses', 'guard_name' => 'student']));
        // $student->givePermissionTo(Permission::create(['name' => 'edit courses', 'guard_name' => 'student']));
        // $student->givePermissionTo(Permission::create(['name' => 'add courses', 'guard_name' => 'student']));
        // $student->givePermissionTo(Permission::create(['name' => 'delete courses', 'guard_name' => 'student']));

        /** INSTITUTION ADMIN */
        $faculty->givePermissionTo(Permission::create(['name' => 'watch public video', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'watch private video', 'guard_name' => 'faculty']));
        
        $faculty->givePermissionTo(Permission::create(['name' => 'view own profile', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'edit own profile', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'edit own email', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'edit own password', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'upload own avatar', 'guard_name' => 'faculty']));

        $faculty->givePermissionTo(Permission::create(['name' => 'read roles', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'edit roles', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'add roles', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'delete roles', 'guard_name' => 'faculty']));

        $faculty->givePermissionTo(Permission::create(['name' => 'browse permissions', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'read permissions', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'edit permissions', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'add permissions', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'delete permissions', 'guard_name' => 'faculty']));

        $faculty->givePermissionTo(Permission::create(['name' => 'browse students', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'read students', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'edit students', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'add students', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'delete students', 'guard_name' => 'faculty']));

        $faculty->givePermissionTo(Permission::create(['name' => 'browse courses', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'read courses', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'edit courses', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'add courses', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'delete courses', 'guard_name' => 'faculty']));

        $faculty->givePermissionTo(Permission::create(['name' => 'browse lessons', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'read lessons', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'edit lessons', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'add lessons', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'delete lessons', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'upload lessons video', 'guard_name' => 'faculty']));

        $faculty->givePermissionTo(Permission::create(['name' => 'browse subjects', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'read subjects', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'edit subjects', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'add subjects', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'delete subjects', 'guard_name' => 'faculty']));

        $faculty->givePermissionTo(Permission::create(['name' => 'browse faculty', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'read faculty', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'edit faculty', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'add faculty', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'delete faculty', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'upload my faculty avatar', 'guard_name' => 'faculty']));
        $faculty->givePermissionTo(Permission::create(['name' => 'upload other faculty avatar', 'guard_name' => 'faculty']));


        /** STUDENTS */
        $student->givePermissionTo(Permission::create(['name' => 'watch public video', 'guard_name' => 'student']));
        $student->givePermissionTo(Permission::create(['name' => 'watch private video', 'guard_name' => 'student']));

        $student->givePermissionTo(Permission::create(['name' => 'view own profile', 'guard_name' => 'student']));
        $student->givePermissionTo(Permission::create(['name' => 'edit own profile', 'guard_name' => 'student']));
        $student->givePermissionTo(Permission::create(['name' => 'edit own email', 'guard_name' => 'student']));
        $student->givePermissionTo(Permission::create(['name' => 'edit own password', 'guard_name' => 'student']));
        $student->givePermissionTo(Permission::create(['name' => 'upload own avatar', 'guard_name' => 'student']));

        $student->givePermissionTo(Permission::create(['name' => 'browse courses', 'guard_name' => 'student']));
        $student->givePermissionTo(Permission::create(['name' => 'read courses', 'guard_name' => 'student']));

        $student->givePermissionTo(Permission::create(['name' => 'browse lessons', 'guard_name' => 'student']));
        $student->givePermissionTo(Permission::create(['name' => 'read lessons', 'guard_name' => 'student']));

        $student->givePermissionTo(Permission::create(['name' => 'browse subjects', 'guard_name' => 'student']));
        $student->givePermissionTo(Permission::create(['name' => 'read subjects', 'guard_name' => 'student']));
    }
}
