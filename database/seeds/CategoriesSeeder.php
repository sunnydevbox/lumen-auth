<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c = collect([
            'Development' => [
                'Web Development',
                'Data Science',
                'Mobile Apps',
            ],
            'Business' => [
                'Finance',
                'Entrepreneurship',
            ],
            'Finance & Accounting' => [
                'Accounting & Bookkeeping',
                'Compliance'
            ],
            'IT & Software' => [
                'IT Certification',
                'Network & Security',
            ],
            'Office Productivity' => [
                'Microsoft',
                'Apple',
            ],
            'Personal Development' => [
                'Personal Transformation',
                'Productivity'
            ],
            'Design' => [
                'Web Design',
                'Graphic Design'
            ],
            'Marketing' => [
                'Digital Marketing',
                'Search Engine Optimization'
            ],
            'Lifestyle' => [
                'Arts && Crafts',
                'Food & Beverage'
            ],
            'Photography' => [
                'Digital Photography',
                'Portraits',
            ],
            'Health & Fitness' => [
                'Fitness',
                'General Health',
                'Sports',
                'Nutrition',
            ],
            'Music' => [
                'Instruments',
                'Production',
            ],
            'Teaching & Academics' => [
                'Engineering',
                'Humanities',
                'Math',
                'Science',
                'Online Education',
                'Social Science',
                'Language',
                'Teacher Training',
                'Test Prep',
                'Other Teaching Academmics',
            ],
        ])->each(function($subs, $key){
            $parent = Category::create([
                'label' => $key,
                'status' => 1
            ]);

            collect($subs)->each(function($item, $key) use ($parent) {
                if (is_string($item)) {
                    $parent->appendNode(Category::create([
                        'label' => $item,
                        'status' => 1
                    ]));
                }
            });
           
            // dd($item,$key);
            
        });



        // $super = Category::create($c); // System/Dev roles
    }
}
