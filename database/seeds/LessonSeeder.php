<?php

use Illuminate\Database\Seeder;
use App\Models\Lesson;
use App\Models\Subject;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subject = Subject::first();
        Lesson::create([
            'code' => '123',
            'subject_id' => $subject->uuid,
            'name' => 'Introduction to Physics',
            'description' => 'The quick brown fox fumped over the lazy dog',
            'video_duration' => 58,
            'status' => 'active',
        ]);

        Lesson::create([
            'code' => '213',
            'subject_id' => $subject->uuid,
            'name' => 'Requirements',
            'description' => 'The quick brown fox fumped over the lazy dog',
            'video_duration' => 43,
            'status' => 'active',
        ]);

        // dd(Subject::where('status', 'active')->inRandomOrder()->first()->uuid);
        factory(Lesson::class, 30)->create()->each(function ($object) {
            // dd($object)
            // dd(Subject::where('status', 'active')->first());
            // dd(Subject::where('status', 'active')->inRandomOrder()->first()->uuid);
            $object->subject_id = Subject::where('status', 'active')->inRandomOrder()->first()->uuid;
            $object->save();
        });
    }
}
