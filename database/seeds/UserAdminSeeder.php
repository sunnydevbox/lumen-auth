<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Faculty;
use App\Models\Role;
use Faker\Generator as Faker;


class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {  
        $admin = User::create([
            'email' => 'admin@admin.com',
            'password' => '123qwe',
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'status'    => 'active',
            'dob' => $faker->dateTime(),
            'gender' => rand(0,1),
            'address_1' => $faker->streetAddress,
            'city' => $faker->city,
            'zipcode' => $faker->postCode,
            'state' => $faker->state,
            'country' => $faker->country,
        ]);


        $instAdmin = Faculty::create([
            'email' => 'institutionadmin@admin.com',
            'password' => '123qwe',
            'first_name' => 'School',
            'last_name' => 'Admin',
            'status'    => 'active',
            'dob' => $faker->dateTime(),
            'gender' => rand(0,1),
            'address_1' => $faker->streetAddress,
            'city' => $faker->city,
            'zipcode' => $faker->postCode,
            'state' => $faker->state,
            'country' => $faker->country,
        ]);

        $faculty = Role::where(['name' => 'faculty', 'guard_name' => 'faculty'])->first(); 

        $instAdmin->assignRole($faculty);
        $admin->assignRole('super');


        factory(Faculty::class, 15)->create()->each(function ($user) use ($faculty) {
            $user->assignRole($faculty);
        });
    }
}
