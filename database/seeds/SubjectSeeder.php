<?php

use Illuminate\Database\Seeder;
use App\Models\Subject;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
            'code' => 'PHY101',
            'name' => 'Physics 101',
            'description' => 'The quick brown fox jumped over the lazy dog'
        ]);

        factory(Subject::class, 50)->create()->each(function ($object) {
            $object->save();
        });
    }
}
