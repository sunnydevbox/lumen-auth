<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Lesson;

class LessonCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lesson::all()->each(function($lesson) {
            $lesson->categories()
                ->attach(
                    Category::inRandomOrder()
                        ->limit(rand(1,3))
                        ->get()
                );
        });
    }
}
