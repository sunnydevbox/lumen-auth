<?php

use Illuminate\Database\Seeder;
use App\Models\Student;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $student = Student::create([
            'first_name' => 'Student',
            'last_name' => 'Student',
            'id' => 123456,
            'status' => 'enrolled',
            'dob' => $faker->dateTime(),
            'gender' => Arr::random([0,1]),
            'password' => Hash::make('123qwe'),
            'address_1' => $faker->streetAddress,
            'city' => $faker->city,
            'zipcode' => $faker->postCode,
            'state' => $faker->state,
            'country' => $faker->country,
            'birth_place' => $faker->state,
        ]);

        $student->assignRole('student');     

        $role = \App\Models\Role::findByName('student','student');

        factory(Student::class, 50)->create()->each(function ($user) use ($role) {
            $user->assignRole($role);
        });
        
    }
}
