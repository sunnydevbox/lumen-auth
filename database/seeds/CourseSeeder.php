<?php

use Illuminate\Database\Seeder;
use App\Models\Course;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::create([
            'code' => 'BS-CompE',
            'name' => 'B.S. Computer Engineering',
            'description' => 'The quick brown fox jumped over the lazy dog'
        ]);

        Course::create([
            'code' => 'BS-IT',
            'name' => 'B.S. Information Technology',
            'description' => 'The quick brown fox jumped over the lazy dog'
        ]);

        Course::create([
            'code' => 'BS-Nursing',
            'name' => 'B.S. Nursing',
            'description' => 'The quick brown fox jumped over the lazy dog'
        ]);
    }
}
