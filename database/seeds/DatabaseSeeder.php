<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('RoleSeeder');
        $this->call('PermissionSeeder');
        
        $this->call('CourseSeeder');
        $this->call('SubjectSeeder');
        $this->call('LessonSeeder');

        $this->call('CategoriesSeeder');
        $this->call('LessonCategoriesSeeder');

        $this->call('UserAdminSeeder');
    }
}
