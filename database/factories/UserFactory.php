<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use App\Models\Student;
use App\Models\Faculty;
use App\Models\Subject;
use App\Models\Lesson;
use App\Models\Course;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$statuses = ['active', 'inactive', 'pending'];
$userStatuses = ['active', 'inactive', 'pending','suspended','banned'];
$studentStatuses = ['pending', 'enrolled', 'suspended', 'dropped'];
$factory->define(User::class, function (Faker $faker) use ($userStatuses) {
    return [
        'first_name' => $faker->firstName,
        'middle_name' => $faker->lastName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => Hash::make('123qwe'),
        'status' => Arr::random($userStatuses),
        'dob' => $faker->dateTime(),
        'gender' => Arr::random([0,1]),
        'address_1' => $faker->streetAddress,
        'city' => $faker->city,
        'zipcode' => $faker->postCode,
        'state' => $faker->state,
        'country' => $faker->country,
        'birth_place' => $faker->state,
    ];
});


$factory->define(Student::class, function (Faker $faker) use ($studentStatuses) {
    return [
        'email' => $faker->email,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'middle_name' => $faker->lastName,
        'id' => Str::random(6),
        'status' => Arr::random($studentStatuses),
        'dob' => $faker->dateTime(),
        'gender' => Arr::random([0,1]),
        'password' => Hash::make('123qwe'),
        'address_1' => $faker->streetAddress,
        'city' => $faker->city,
        'zipcode' => $faker->postCode,
        'state' => $faker->state,
        'country' => $faker->country,
        'birth_place' => $faker->state,
    ];
});


$factory->define(Faculty::class, function (Faker $faker) use ($userStatuses) {
    return [
        'email' => $faker->email,
        'first_name' => $faker->firstName,
        'middle_name' => $faker->lastName,
        'last_name' => $faker->lastName,
        'status' => Arr::random($userStatuses),
        'dob' => $faker->dateTime(),
        'gender' => rand(0, 1),
        'password' => Hash::make('123qwe'),
        'address_1' => $faker->streetAddress,
        'city' => $faker->city,
        'zipcode' => $faker->postCode,
        'state' => $faker->state,
        'country' => $faker->country,
        'birth_place' => $faker->state,
    ];
});


$factory->define(Subject::class, function (Faker $faker) use ($statuses) {

    return [
        'code' => 'SUBJ_' . $faker->swiftBicNumber,
        'name' => ucwords($faker->words(rand(1,3), true)),
        'description' => $faker->sentences(rand(1,3), true),
        'status' => Arr::random($statuses),
    ];
});

$factory->define(Lesson::class, function (Faker $faker) use ($statuses) {

    return [
        'code' => 'LES_' . $faker->swiftBicNumber,
        'name' => ucwords($faker->words(rand(1,3), true)),
        'description' => $faker->sentences(rand(1,3), true),
        'status' => Arr::random($statuses),
        // 'duration' => $faker->randomDigitNotNull,
    ];
});

// use App\Models\Lesson;
// use App\Models\Course;