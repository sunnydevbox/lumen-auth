<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('me', 'AuthController@me');
$router->post('login', 'AuthController@login');
$router->post('logout', 'AuthController@logout');
$router->post('reset-password', 'AuthController@resetPassword');
$router->post('register', 'AuthController@register');

// Videos
$router->group([
    'prefix' => 'classes',
], function($router) {
    $router->get('/', 'LessonController@publicIndex');
});

// Search
$router->group([
    'prefix' => 'search',
], function($router) {
    $router->get('/', 'SearchController@searchClasses');
});



// dd(config('attachments.routes'));
$router->group([
    'prefix' => config('attachments.routes.prefix'),
    'middleware' => config('attachments.routes.middleware')
], function () use($router) {
    $router->get(config('attachments.routes.shared_pattern'), [
        'as' => 'attachments.download-shared',
        'uses' => '\Bnb\Laravel\Attachments\Http\Controllers\ShareController@download'
    ]);
    $router->get(config('attachments.routes.pattern'), [
        'as' => 'attachments.download',
        'uses' => '\Bnb\Laravel\Attachments\Http\Controllers\DownloadController@download'
    ]);

    $router->post(config('attachments.routes.dropzone.upload_pattern'), [
        'as' => 'attachments.dropzone',
        'uses' => '\Bnb\Laravel\Attachments\Http\Controllers\DropzoneController@post'
    ]);

    $router->delete(config('attachments.routes.dropzone.delete_pattern'), [
        'as' => 'attachments.dropzone.delete',
        'uses' => '\Bnb\Laravel\Attachments\Http\Controllers\DropzoneController@delete'
    ]);
});

$router->group([

   // 'middleware' => 'auth:api',
    // 'prefix' => 'auth'

], function ($router) {
    
    $router->post('refresh', 'AuthController@refresh');  
    $router->post('me', 'AuthController@me');
    

    $router->group([
        'prefix' => 'user',
    ], function($router) {
        $router->post('/', 'UserController@create');
        $router->get('/', 'UserController@index');
        $router->get('/{id}', 'UserController@show');
        $router->delete('/{id}', 'UserController@destroy');
        $router->put('/{id}', 'UserController@update');

        // $router->put('passowrd/{id}', 'UserController@update');

    });

    $router->group([
        'prefix' => 'profile',
    ], function($router) {
        $router->get('/{id}', 'ProfileController@show');
        $router->put('/{id}', 'ProfileController@update');
        $router->post('/update-email/{id}', 'ProfileController@updateEmail');
        $router->post('/update-password/{id}', 'ProfileController@updatePassword');
        $router->post('/upload-avatar/{id}', 'ProfileController@uploadAvatar');
    });
    
    
    $router->group([
        'prefix' => 'courses',
    ], function($router) {
        $router->post('/', 'CourseController@create');
        $router->get('/', 'CourseController@index');
        $router->get('/{id}', 'CourseController@show');
        $router->delete('/{id}', 'CourseController@destroy');
        $router->put('/{id}', 'CourseController@update');
    });

    $router->group([
        'prefix' => 'subjects',
    ], function($router) {
        $router->post('/', 'SubjectController@create');
        $router->get('/', 'SubjectController@index');
        $router->get('/{id}', 'SubjectController@show');
        $router->delete('/{id}', 'SubjectController@destroy');
        $router->put('/{id}', 'SubjectController@update');
    });

    $router->group([
        'prefix' => 'lessons',
    ], function($router) {
        $router->get('/video-by-code/{code}', 'LessonController@getByCode');
        $router->get('/video/{key}', 'LessonController@getVideo');
        $router->post('/upload-video', 'LessonController@uploadVideo');
        $router->post('/', 'LessonController@create');
        $router->get('/', 'LessonController@index');
        $router->get('/{id}', 'LessonController@show');
        $router->delete('/{id}', 'LessonController@destroy');
        $router->put('/{id}', 'LessonController@update');
    });

    $router->group([
        'prefix' => 'faculty',
    ], function($router) {
        $router->post('/', 'FacultyController@create');
        $router->get('/', 'FacultyController@index');
        $router->get('/{id}', 'FacultyController@show');
        $router->delete('/{id}', 'FacultyController@destroy');
        $router->put('/{id}', 'FacultyController@update');
        $router->post('/upload-avatar/{id}', 'FacultyController@uploadAvatar');
    });

    $router->group([
        'prefix' => 'students',
    ], function($router) {
        $router->post('/', 'StudentController@create');
        $router->get('/', 'StudentController@index');
        $router->get('/{id}', 'StudentController@show');
        $router->delete('/{id}', 'StudentController@destroy');
        $router->put('/{id}', 'StudentController@update');
    });

    $router->group([
        'prefix' => 'role',
    ], function($router) {
        $router->post('/', 'RoleController@create');
        $router->get('/', 'RoleController@index');
        $router->get('/{id}', 'RoleController@show');
        $router->delete('/{id}', 'RoleController@destroy');
        $router->put('/{id}', 'RoleController@update');
    });

    $router->group([
        'prefix' => 'permission',
    ], function($router) {
        $router->post('/', 'PermissionController@create');
        $router->get('/', 'PermissionController@index');
        $router->get('/{id}', 'PermissionController@show');
        $router->delete('/{id}', 'PermissionController@destroy');
        $router->put('/{id}', 'PermissionController@update');
    });
});