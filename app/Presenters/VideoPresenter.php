<?php
namespace App\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;

class VideoPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new \App\Transformers\VideoTransformer();
    }
}