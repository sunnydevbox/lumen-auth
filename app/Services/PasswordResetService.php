<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use Webpatser\Uuid\Uuid;

class PasswordResetService extends BaseService
{

    public function generateToken($email)
    {
        $token = $this->repository->findWhere(['email' =>$email])->first();
        
        if ($token) {
            $this->repository->update(['token'=>'saad'], $token->id);
            // $token->token = 'sasd';
            // $token->save();
        } else {
            $token =$this->repository->create([
                'email' => $email,
                'token' => Uuid::generate()->string,
            ]);
        }

        return $token;
    }



    public function repository()
    {
        return app(\App\Repositories\PasswordResetRepository::class);
    }

    public function validator()
    {
        return null;// app(\App\Validators\RoleValidator::class);
    }
}