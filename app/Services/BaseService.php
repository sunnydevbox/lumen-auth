<?php
namespace App\Services;

use \App\Repositories\UserRepository;
use \App\Validators\UserValidator;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

abstract class BaseService
{

    public function create($data)
    {
        try {            
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $result = $this->repository->create($data);
            
            return $result;
        } catch(Exception $e) {
            throw $e;
        }
    }

    public function update($data, $id)
    {
        try {
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $model = $this->repository->skipPresenter()->find($id);
            $model->fill($data);
            $result = $model->update();
            
            return $model;
        } catch(Exception $e) {
            throw $e;
        }
    }


    public function destroy($id)
    {
        try {
            $result = $this->repository->delete($id);
            
            return $result;
        } 
        catch(Exception $e) {
            $message = '';
            $code = 400;

            if ($e instanceof ModelNotFoundException) {
                $message = 'resource not found';
                $code = 404;
            }

            throw new Exception($message, $code);
        }
    }

    public function show($id)
    {
        try {
            $result = $this->repository->skipPresenter()->find($id);
            return $result;
        } 
        catch(Exception $e) {
            $message = '';
            $code = 400;

            if ($e instanceof ModelNotFoundException) {
                $message = 'resource not found';
                $code = 404;
            }

            throw $e;
            throw new Exception($message, $code);
        }
    }

    public function index($data)
    {
        $this->repository->pushCriteria(\App\Criteria\RequestCriteria::class);

        if (isset($data['limit']) && $data['limit'] == 0) {
            $result = $this->repository->all();
        } else {
            $limit = isset($data['limit']) ? $data['limit'] : config('repository.pagination.limit', 15);
            $result = $this->repository->paginate($limit);
        }

        return $result;
    }


    abstract public function repository();
    abstract public function validator();

    public function __construct()
    {
        $this->repository = $this->repository();
        $this->validator = $this->validator();
        $this->user = auth();
    }
}