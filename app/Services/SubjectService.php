<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class SubjectService extends BaseService
{
    public function index($data)
    {
        $user = user();

        if ($user && $user->can('browse subjects')) {
            return parent::index($data);
        } else {

        }
    }

    public function repository()
    {
        return app(\App\Repositories\SubjectRepository::class);
    }

    public function validator()
    {
        return app(\App\Validators\SubjectValidator::class);
    }
}