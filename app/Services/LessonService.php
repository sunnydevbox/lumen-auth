<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use Illuminate\Support\Facades\Storage;
use App\Services\JWPlayerService;
use Carbon\Carbon;

class LessonService extends BaseService
{

    /** START JWP VIDEO  */
    public function getByCode($code)
    {
        // set permission
        $this->repository->makePresenter(\App\Presenters\VideoPresenter::class);
        $v = $this->repository->findByField('code', $code);

        if (count($v['data'])) {
            return $v['data'][0];
        }

        throw new ModelNotFoundException();
    }

    public function getVideoURL($videoKey)
    {
        return $this->jwp->getMedia($videoKey);    
    }

    public function getVideoPosterURL($videoKey, $size = 480)
    {
        $widths = [40, 120, 320, 480, 640, 720, 1280, 1920];
        $new = collect($widths)
                ->reduce(function($new, $width) use ($videoKey) {
                    
                    $new[$width] = "https://cdn.jwplayer.com/thumbs/{$videoKey}-{$width}.jpg";
                    // dd($width,$new);
                    return $new;
                });

        return $new;    
    }

    
    public function getVideo($key)
    {   
        // set permission
        $v = 
            $this->repository
                ->skipPresenter()
                ->findByField('video_key', $key);
                
        if ($v->count()) {
            $v = $v->first();

            $jwpVideo = $this->jwp->show($key);

            $attr = [
                    'video_status' => strtolower($jwpVideo->status),
                    'video_duration' => $jwpVideo->video->duration,
                    // 'video_key' => 'QUGZXZSx'
                    // 'video_url' => null
                    'video_size' => $jwpVideo->video->size,
                    // 'video_embed' => null
                    'video_created' => Carbon::createFromTimestamp($jwpVideo->video->date),
                    'video_updated' => Carbon::createFromTimestamp($jwpVideo->video->updated)
            ];

            $v->update($attr);
            // dd($v);
            return $v;
        }
    }

    public function uploadVideo($uuid, $file)
    {
        $user = user();
        try {
            if ($user && $user->can('upload lessons video')) {

                $lesson = $this->repository->skipPresenter()->find($uuid);
                
                if (!$lesson) {
                    throw new Exception('invalid_object', 400);
                }

                $upload = $this->jwp->upload($file, ['title' => 'Fox', 'description'=> 'quick brown']);
                
                // UPDATE record after uploading
                $lesson->video_key = $upload['media']['key'];
                $lesson->update();

                return $lesson->video_key;

            } else {
                throw new Exception('not_authorized',401);
            }
        } catch(Exception $e) {
            throw $e;
        }
        
    }

    /** END JWP VIDEO  */
    


    /** AUTHENTICATED **/
    public function index($request)
    {
        $user = user();

        if ($user && $user->can('browse lessons')) {
            return parent::index($request);
        }
    }

    public function repository()
    {
        return app(\App\Repositories\LessonRepository::class);
    }

    public function validator()
    {
        return app(\App\Validators\LessonValidator::class);
    }


    /** UNAUTHORIZED **/

    public function publicIndex($request)
    {
        return parent::index($request);
    }

    public function __construct(
        JWPlayerService $jwp
    ) {
        parent::__construct();
        $this->jwp = $jwp;  
    }
}