<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class CourseService extends BaseService
{
    public function index($request)
    {
        try {
            $user = user();

            if ($user && $user->can('browse courses')) {
                return parent::index($request);
            } else {
                throw new Exception('not_authorized',401); 
            }
        } catch(Exception $e) {
            throw $e;
        }
    }

    public function repository()
    {
        return app(\App\Repositories\CourseRepository::class);
    }

    public function validator()
    {
        return app(\App\Validators\CourseValidator::class);
    }
}