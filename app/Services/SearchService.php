<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Exception;

class SearchService extends BaseService
{
    public function searchClasses($data)
    {   
        $request = app('request');
        $request->request->add(['search' => "name:{$data['q']};description:{$data['q']}" ]);

        // dd($request->all());
        
        $this->rpoLesson->pushCriteria(\App\Criteria\RequestCriteria::class);

        if (isset($data['limit']) && $data['limit'] == 0) {
            $result = $this->rpoLesson->all();
        } else {
            $limit = isset($data['limit']) ? $data['limit'] : config('repository.pagination.limit', 15);
            $result = $this->rpoLesson->paginate($limit);
        }

        
        dd($result);
        // $this->rpoLesson->
    }



    public function __construct()
    {
        parent::__construct();

        $this->rpoLesson = app(\App\Repositories\LessonRepository::class);
    }


    public function repository()
    {
        return null; // app(\App\Repositories\CategoryRepository::class);
    }

    public function validator()
    {
        return null; // app(\App\Validators\CategoryValidator::class);
    }
}