<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\Services\PasswordResetService;
use App\Events\UserRegisteredEvent;

class AuthService extends BaseService
{
    public function resetPassword($email)
    {
        $user = $this->repository->findWhere(['email' => $email])->first();
        
        if ($user) {
            $token = $this->passwordResetService->generateToken($user->email);
        }
    }

    public function register($type, $data)
    {
        
        try {
            
            $user = $this->repository->create($data);
            // dd($type, $data);
            if(!$user->hasRole($type)){
                $user->syncRoles($type);
            }
            // $user->assignRole($type);
            event(new UserRegisteredEvent($user));

            return $user;
        } catch(Exception $e){
            throw $e;
        }


    }



    public function repository()
    {
        return app(\App\Repositories\UserRepository::class);
    }

    public function validator()
    {
        return app(\App\Validators\UserValidator::class);
    }
    
    public function __construct(PasswordResetService $passwordResetService)
    {
        parent::__construct();

        $this->passwordResetService = $passwordResetService;
    }
}