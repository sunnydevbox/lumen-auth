<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use Tymon\JWTAuth\PayloadFactory;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Facades\JWTAuth;


class JWPlayerService
{
    public function embedJs($key)
    {

    }


    private function generateToken($path, $exp, $secret = null) 
    {
        // {path}:{exp}:{secret}"
        $secret = $secret ? $secret : env('JWPLAYER_API_SECRET');
        $payload = "{$path}:{$exp}:{$secret}";
        
        $token = md5($payload);

        return $token;
    }

    private function generateSignedUrl($baseUrl, $path, $exp)
    {
        $token = $this->generateToken($path, $exp);

        $url = "{$baseUrl}{$path}?exp={$exp}&sig={$token}";
        return $url;
    }

    public function getMedia($videoKey)
    {
        $exp = \Carbon\Carbon::now()->addSeconds(10)->timestamp;
        return $this->generateSignedUrl('https://cdn.jwplayer.com/', "videos/{$videoKey}.mp4", $exp);
    }

    public function show($videoKey)
    {
        // https://cdn.jwplayer.com/v2/media/media_id

        $response = json_encode($this->jwp->call("/videos/show/", ['video_key' => $videoKey ], 'v1'));
        dd(json_decode($response));
        return json_decode($response);
    }


    public function upload($file, $meta = [])
    {
        $videoFile = storage_path('app/'.$file->store('temp'));
        
        $jwp = new \Jwplayer\JwplatformAPI(env('JWPLAYER_API_KEY'), env('JWPLAYER_API_SECRET'));

        $params = array();
        $params['title'] = 'PHP API Test Upload';
        $params['description'] = 'Video description here';

        // Create the example video
        $response = json_encode($jwp->call('/videos/create', $params));

        $decoded = json_decode(trim($response), TRUE);

        $uploadResponse = $this->jwp->upload($videoFile, $decoded['link']);
        unlink($videoFile);

        return $uploadResponse;
    }

    public function update($code)
    {
        // return app(\App\Validators\UserValidator::class);
    }

    public function __construct()
    {
        // dd(new \Jwplayer\JwplatformAPI(env('JWPLAYER_API_KEY'), env('JWPLAYER_API_SECRET')));
        $this->jwp = new \Jwplayer\JwplatformAPI(env('JWPLAYER_API_KEY'), env('JWPLAYER_API_SECRET'), 'deEj8GhYulc0mHcP1xEEjGInV2xwdldqWm1ZWFZYTUhjelQwWlVkemhRVm01UlZXWjIn');
    }
}