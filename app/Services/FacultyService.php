<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class FacultyService extends BaseService
{
    public function repository()
    {
        return app(\App\Repositories\FacultyRepository::class);
    }

    public function validator()
    {
        return app(\App\Validators\FacultyValidator::class);
    }


    // public function __construct(
    //     UserRepository $repository,
    //     UserValidator $validator
    // ) {
    //     $this->repository = $repository;
    //     $this->validator = $validator;
    // }
}