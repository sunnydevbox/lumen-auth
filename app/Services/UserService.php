<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class UserService extends BaseService
{
    public function repository()
    {
        return app(\App\Repositories\UserRepository::class);
    }

    public function validator()
    {
        return app(\App\Validators\UserValidator::class);
    }


    // public function __construct(
    //     UserRepository $repository,
    //     UserValidator $validator
    // ) {
    //     $this->repository = $repository;
    //     $this->validator = $validator;
    // }
}