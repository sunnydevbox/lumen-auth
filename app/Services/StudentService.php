<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class StudentService extends BaseService
{
    public function index($request)
    {
        $user = auth();

        try {
            if ($user && $user->can('browse students')) {
                return parent::index($request);
            } else {
                throw new Exception('not_authorized',401);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function repository()
    {
        return app(\App\Repositories\StudentRepository::class);
    }

    public function validator()
    {
        return app(\App\Validators\StudentValidator::class);
    }
}