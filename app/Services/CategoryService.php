<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;

class CategoryService extends BaseService
{
    public function repository()
    {
        return app(\App\Repositories\CategoryRepository::class);
    }

    public function validator()
    {
        return app(\App\Validators\CategoryValidator::class);
    }
}