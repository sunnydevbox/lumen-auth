<?php
namespace App\Services;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use \Spatie\Permission\Exceptions\UnauthorizedException;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\Storage;

class ProfileService extends BaseService
{
    public function uploadAvatar($id, UploadedFile $file)
    {
        if ($this->user) {
            if (($id === $this->user->uuid) && $this->user->can('edit own email')) {
                $g = $this->user->attachmentsGroup('avatar');
                $g->each(function($i) {
                    $i->delete();
                });
                
                // $a = $this->user->attachment('avatar');
                // if ($a) {
                //     $a->delete();
                // }

                $a = $this->user->attach($file,[
                    'group' => 'avatar',
                    'key'   => 'original',
                ]);

                $realFile = storage_path('app/'.$a->filepath);

                $img = $this->image->make($realFile);

                // TINY
                $imgSmall = $img->resize(100, 100);
                $pathSmall = storage_path($imgSmall->basename);
                $imgSmall->save($pathSmall);
                $a = $this->user->attach($pathSmall,[
                    'group' => 'avatar',
                    'key'   => 'tiny',
                ]);

                // SMALL
                $imgSmall = $img->resize(320, 240);
                $pathSmall = storage_path($imgSmall->basename);
                $imgSmall->save($pathSmall);
                $a = $this->user->attach($pathSmall,[
                    'group' => 'avatar',
                    'key'   => 'small',
                ]);

                // MEDIUM
                $imgSmall = $img->resize(600, 600);
                $pathSmall = storage_path($imgSmall->basename);
                $imgSmall->save($pathSmall);
                $a = $this->user->attach($pathSmall,[
                    'group' => 'avatar',
                    'key'   => 'small',
                ]);

                // LARGE
                $imgSmall = $img->resize(1024, 1024);
                $pathSmall = storage_path($imgSmall->basename);
                $imgSmall->save($pathSmall);
                $a = $this->user->attach($pathSmall,[
                    'group' => 'avatar',
                    'key'   => 'large',
                ]);
                
                return $this->user->avatar;
            }

            
        }

        throw UnauthorizedException::forPermissions([]);
    }

    public function updateEmail($id, $data)
    {
        if ($this->user) {
            if (($id === $this->user->uuid) && $this->user->can('edit own email')) {

                // VERIFY EMAIL IS NOT DUPLICATE
                $data['email'] = trim($data['email']);
                $this->validator->with($data)->passesOrFail('UPDATE_EMAIL');

                $model = $this->repository->skipPresenter()->find($id);
                
                // VERIFY WITH PASSWORD
                if ($model->confirmPassword($data['password'])) {
                    $model
                        ->fill(['email' => $data['email']])
                        ->update();
                    
                    return true;
                    // SEND EMAIL?
                }

                throw new Exception('invalid_password', 400);
            }
        }

        throw UnauthorizedException::forPermissions([]);
    }

    

    public function updatePassword($id, $data)
    {
        if ($this->user) {
            if (($id === $this->user->uuid) && $this->user->can('edit own password')) {

                $this->validator->with($data)->passesOrFail('UPDATE_PASSWORD');
                
                // VERIFY WITH PASSWORD
                $model = $this->repository->skipPresenter()->find($id);
                if ($model->confirmPassword($data['current_password'])) {

                    $model
                        ->fill(['password' => $data['password']])
                        ->update();
                    
                    return true;
                    // SEND EMAIL?
                }

                throw new Exception('invalid_password', 400);
                
            }
        }

        throw UnauthorizedException::forPermissions([]);
    }

    public function update($data, $id)
    {
        if ($this->user) {
            if (($id === $this->user->uuid) && $this->user->can('edit own profile')) {
                return parent::update($data, $id);
            }
        }

        throw UnauthorizedException::forPermissions([]);
    }

    public function show($id)
    {
        $this->user = auth();

        if ($this->user) {
            if (($id === $this->user->uuid) && $this->user->can('view own profile')) {
                $this->repository->skipPresenter();
                return parent::show($id);
            }
        }

        throw UnauthorizedException::forPermissions([]);
    }

    public function repository()
    {
        $this->user = auth();

        if ($this->user) {
            if ($this->user->guard_name == 'faculty') {
                return app(\App\Repositories\FacultyRepository::class);
            } if ($this->user->guard_name == 'student') {
                return app(\App\Repositories\StudentRepository::class);
            } else {
                return app(\App\Repositories\UserRepository::class);
            }
        }
        return null;
    }

    public function validator()
    {
        $this->user = auth();
        if ($this->user) {
            if ($this->user->guard_name == 'faculty') {
                return app(\App\Validators\FacultyValidator::class);
            } else {
                return app(\App\Validators\UserValidator::class);
            }
        }
    }


    public function __construct(
        ImageManager $image
    ) {
        parent::__construct();
        $this->image = $image;
    }
}