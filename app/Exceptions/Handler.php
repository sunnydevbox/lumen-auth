<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Throwable;
use ReflectionException;
use TypeError;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        // AuthorizationException::class,
        HttpException::class,
        // ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            // return your custom response
            return response()->json(['message' => 'resource_not_found'], 404);
        } else if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json([
                'message' => 'invalid_endpoint',
                'code' => 404,
            ], 404);
        } else if ($exception instanceof ValidatorException) {
            return response()->json([
                'message' => $exception->getMessageBag(),
                'code' => 400,
            ], 400);
        } else if ($exception instanceof TokenExpiredException) {
            return response()->json([
                'message' => 'token_expired',
                'code' => 500,
            ]);
        } else if ($exception instanceof TokenInvalidException) {
            return response()->json([
                'message' => 'token_invalid',
                'code' => 500,
            ]);
        } else if ($exception instanceof JWTException) {
            return response()->json([
                'message' => 'token_absent',
                'code' => 500,
            ]);
        } else if ($exception instanceof UnauthorizedException) {
            return response()->json([
                'message' => 'not_permitted',
                'code' => 403,
            ], 403);
        }
        // else if ($exception instanceof TypeError) {
            
        //     // This is usually is caused when token is invalid or db is not right
        //     if (preg_match('/(JWTGuard)/', $exception->getMessage())) {
        //         return response()->json([
        //             'message' => 'access_denied',
        //             'code' => 401,
        //         ], 401);
        //     }

        //     return response()->json([
        //         'message' => $exception->getMessage(),
        //         'code' => 500,
        //     ], 500);    
        // } 
        else { 
            if (config('app.env') != 'production') {
                if ($exception->getCode() == 400) {
                    return response()->json([
                        'message' => $exception->getMessage(),
                        'code' => $exception->getCode(),
                    ], $exception->getCode());
                }
                
                // $message = $exception->getTraceAsString();
                // $message = $exception->getMessage();

                // dd($message);
            } else {
                $message = 'system_error';
                return response()->json([
                    'message' => $message,
                    'code' => 500,
                ], 500);
            }

            
        }

        return parent::render($request, $exception);
    }
}
