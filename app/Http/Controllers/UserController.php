<?php
namespace App\Http\Controllers;

use Tymon\JWTAuth\JWTAuth;
use \App\Services\UserService;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    public function __construct(
        JWTAuth $jwt,
        UserService $service,
        UserRepository $repository
    ) {
        parent::__construct($jwt);
        $this->jwt = $jwt;
        $this->service = $service;
        $this->repository = $repository;
        
        $this->middleware('auth:api');
    }


}
