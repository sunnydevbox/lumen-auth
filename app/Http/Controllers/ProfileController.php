<?php
namespace App\Http\Controllers;

use App\Services\ProfileService;
use Illuminate\Http\Request; 

class ProfileController extends BaseController
{
    public function uploadAvatar(Request $request, $id)
    {
        $this->service->uploadAvatar($id, $request->file('avatar'));
    }


    public function updateEmail(Request $request, $id)
    {
        if ($this->service->updateEmail($id, $request->only(['email', 'password']))) {
            return response(null, 200);
        } 
        return response()->json(['message' => 'update_email_failed'], 400);
    }

    public function updatePassword(Request $request, $id)
    {
        if ($this->service->updatePassword($id, $request->all())) {
            return response(null, 200);
        } 
        
        return response()->json(['message' => 'update_password_failed'], 400);
    }

    public function __construct(
        ProfileService $service
    ) {
        $this->service = $service;
        //         $this->middleware('auth:student', 
        //     [
        //         'except' => [
        //             'login', 
        //             'resetPassword', 
        //             'register'
        //         ]
        //     ]
        // );
    }
}
