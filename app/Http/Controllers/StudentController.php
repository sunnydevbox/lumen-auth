<?php
namespace App\Http\Controllers;

use Tymon\JWTAuth\JWTAuth;
use App\Services\StudentService;

class StudentController extends BaseController
{
    public function __construct(
        StudentService $service
    ) {
        $this->service = $service;
        
        // $this->middleware('auth:student');
    }
}
