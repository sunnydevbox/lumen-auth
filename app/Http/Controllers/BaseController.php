<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use \Illuminate\Database\QueryException;
use \Prettus\Validator\Exceptions\ValidatorException;
use Exception;
use ErrorException;
use BadMethodCallException;

abstract class BaseController extends \Laravel\Lumen\Routing\Controller
{
    public $service = null;
    // public function __construct(
    //     JWTAuth $jwt,
    //     UserService $service,
    //     UserRepository $repository
    // ) {
    //     $this->jwt = $jwt;
    //     $this->service = $service;
    //     $this->repository = $repository;
        
    //     $this->middleware('auth:api');
    // }

    public function __construct() { }
  
    public function create(Request $request)
    {
        $message = '';
        $code = 400;
        try {
            $result = $this->service->create($request->all());
            return response()->json($result);
        } catch (Exception $e) {
            return $this->_response($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $result = $this->service->update( $request->all(), $id);
            return response()->json($result);
        } catch(Exception $e) { 
            return $this->_response($e);
        }
    }

    public function index(Request $request)
    {
        $message = '';
        $code = 400;

        $result = $this->service->index($request->all());
        return response()->json($result);
    }

    public function show(Request $request, $id)
    {
        try {
            $result = $this->service->show($id);
            return response()->json($result);
        }  catch (Exception $e) {
            return $this->_response($e);
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            $result = $this->service->destroy($id);
            
            return response(null, 204);;
        }  catch (Exception $e) {
            return $this->_response($e);
        }
    }

    public function _response($e)
    {
        $message = '';
        $code = 400; 

        if ($e instanceof QueryException) {
            // INVALID query
            // should this be 500 system error?
            $message = $e->getMessage();
            $code = 500;
        } 
        
        // else if ($e instanceof ValidatorException) {
        //     $message = $e->getMessageBag();
        // } 
        
        else if (
            $e instanceof ErrorException
            || $e instanceof BadMethodCallException
        ) {
            $message = $e->getMessage();
            $code = 500;
        }  else {
            throw $e;
        }

        return response()->json([
            'message' => $message,
            'code' => $code,
        ], $code);
    }

  
}
