<?php
namespace App\Http\Controllers;

use Tymon\JWTAuth\JWTAuth;
use App\Services\LessonService;
use Illuminate\Http\Request;

class LessonController extends BaseController
{
    public function getByCode(Request $request, $code)
    {
        $result = $this->service->getByCode($code);
        return response()->json( $result);
    }
    
    public function getVideo(Request $request, $key)
    {
        $this->service->getVideo($key);
    }

    public function uploadVideo(Request $request)
    {
        $result = $this->service->uploadVideo($request->get('uuid'), $request->file('video'));

        return response()->json(['video_key' => $result]);
    }


    public function publicIndex(Request $request)
    {
        $result = $this->service->publicIndex($request->all());
        return response()->json($result);
    }

    public function __construct(
        LessonService $service
    ) {
        $this->service = $service;
    }
}
