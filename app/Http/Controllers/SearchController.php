<?php
namespace App\Http\Controllers;

use App\Services\SearchService;
use Illuminate\Http\Request; 

class SearchController extends BaseController
{
    public function searchClasses(Request $request)
    {
        $result = $this->service->searchClasses($request->all());
        // dd($result);
    }

    public function __construct(
        SearchService $service
    ) {
        $this->service = $service;
    }
}
