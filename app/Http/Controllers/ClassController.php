<?php
namespace App\Http\Controllers;

use App\Services\LessonService;

class ClassController extends BaseController
{
    public function __construct(
        CourseService $service
    ) {
        $this->service = $service;
    }
}
