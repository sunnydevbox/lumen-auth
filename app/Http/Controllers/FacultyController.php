<?php
namespace App\Http\Controllers;

use \App\Services\FacultyService;
use Illuminate\Http\Request;
use \App\Models\Faculty;

class FacultyController extends BaseController
{
    public function uploadAvatar(Request $request, $id)
    {
        $user = Faculty::find($id);
        
        // dd($id, $request->file('avatar'));
        $path = $request->file('avatar');

        $a = $user->attachment('avatar');
        // dd($a);

        if ($a) {
            $a->delete();
        }

        $a = $user->attach($path,[
            'key' => 'avatar',
        ]);

        // Genearate different sizes;
        

        return response()->json(['url' => $a->url]);
        return $path;
    }


    public function __construct(
        FacultyService $service
    ) {
        $this->service = $service;
    }
}
