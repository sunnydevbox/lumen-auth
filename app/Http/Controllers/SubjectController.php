<?php
namespace App\Http\Controllers;

use Tymon\JWTAuth\JWTAuth;
use App\Services\SubjectService;

class SubjectController extends BaseController
{
    public function __construct(
        SubjectService $service
    ) {
        $this->service = $service;
        
        // $this->middleware('auth:student');
    }
}
