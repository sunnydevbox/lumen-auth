<?php
namespace App\Http\Controllers;

use Tymon\JWTAuth\JWTAuth;
use App\Services\RoleService;

class RoleController extends BaseController
{
    public function __construct(
        JWTAuth $jwt,
        RoleService $service
    ) {
        parent::__construct($jwt);
        $this->service = $service;
        
        $this->middleware('auth:api');
    }
}
