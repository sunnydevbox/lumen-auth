<?php
namespace App\Http\Controllers;

use Tymon\JWTAuth\JWTAuth;
use \App\Services\PermissionService;

class PermissionController extends BaseController
{
    public function __construct(
        JWTAuth $jwt,
        PermissionService $service
    ) {
        parent::__construct($jwt);
        $this->jwt = $jwt;
        $this->service = $service;
        
        $this->middleware('auth:api');
    }


}
