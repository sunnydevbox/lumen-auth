<?php

namespace App\Http\Controllers;

// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\JWTAuth;
use Exception;
use App\Services\AuthService;
use App\Repositories\UserRepository;
use App\Repositories\StudentRepository;
use Auth;

class AuthController extends BaseController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(
        JWTAuth $jwt,
        AuthService $service,
        UserRepository $repository
    ) {
        $this->repository = $repository;
        $this->jwt = $jwt;
        $this->service = $service;
        
        // $this->middleware('auth:student', 
        //     [
        //         'except' => [
        //             'login', 
        //             'resetPassword', 
        //             'register'
        //         ]
        //     ]
        // );
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);
        $validation = $request->only(['email', 'password', 'type']);

        try {
            $this->repository->makeValidator()->with($validation)->passesOrFail('LOGIN');
            $guardName = strtolower(trim($request->get('type')));
            switch($guardName) {
                case 'student':
                    $credentials['id'] = $credentials['email'];
                    unset($credentials['email']);
                break;
            }


            $token = app('auth')->guard($guardName)->attempt($credentials);
            
            if (! $token) {
                throw new Exception('invalid_credentials', 400);
            }

        } catch (Exception $e) {
            return $this->_response($e);
        }

    ;
        return response()->json(compact('token'));
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = user();

        if ($user) {
            $roles = $user->getRoleNames();
            $permissions = $user->getAllPermissions()->pluck('name')->toArray();
            $avatar = $user->attachment('avatar');
            $user = $user->toArray();
            $user['roles'] = $roles;
            $user['permissions'] = $permissions;
            $user['avatar'] = $avatar ? $avatar->url : null;
            unset($user['attachments']);

            return response()->json($user);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->jwt->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->jwt->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function resetPassword(Request $request)
    {
        try {
            if ($request->has('email')) {
                $result = $this->service->resetPassword($request->get('email'));
            }
        } catch (Exception $e) {

        }
    }

    public function register(Request $request)
    {
        try {
            $result = $this->service->register($request->get('type'), $request->only(['email', 'password']));
            return response($result, 200);
        }  catch (Exception $e) {
            return $this->_response($e);
        }
    }
}