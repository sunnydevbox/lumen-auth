<?php
namespace App\Validators;

use \Prettus\Validator\LaravelValidator;
use \Prettus\Validator\Contracts\ValidatorInterface;

class StudentValidator extends LaravelValidator
{
    protected $attributes = [
        'email' => 'E-mail',
        'first_name' => 'First Name',
    ];

    protected $messages = [
        'type.required' => 'Login type is invalid'
    ];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'id' => 'required|unique:students',
            'first_name' => 'min:2',
            'last_name' => 'min:2',
            'middle_name' => 'min:2',
            'password'  => 'min:8',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'id' => 'unique:students',
            'password'  => 'min:8',
            'first_name' => 'min:2',
            'last_name' => 'min:2',
            'middle_name' => 'min:2',
            'password'  => 'min:8',
        ],
   ];
}