<?php
namespace App\Validators;

use \Prettus\Validator\LaravelValidator;
use \Prettus\Validator\Contracts\ValidatorInterface;

class SubjectValidator extends LaravelValidator
{
    protected $attributes = [];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'code' => 'unique:courses',
            'name' => 'required|min:3',
            'description' => '',
            'status' => '',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'min:3|unique:roles',
        ]
   ];
}