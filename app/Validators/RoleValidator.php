<?php
namespace App\Validators;

use \Prettus\Validator\LaravelValidator;
use \Prettus\Validator\Contracts\ValidatorInterface;

class RoleValidator extends LaravelValidator
{
    protected $attributes = [];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|min:3|unique:roles',
            'guard_name' => 'min:2|in:web,api',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'min:3|unique:roles',
            'guard_name' => 'min:2|in:web,api',
        ]
   ];
}