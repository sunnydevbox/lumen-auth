<?php
namespace App\Validators;

use \Prettus\Validator\LaravelValidator;
use \Prettus\Validator\Contracts\ValidatorInterface;

class LessonValidator extends LaravelValidator
{
    protected $attributes = [
        'subject_id' => 'Subject'
    ];

    protected $messages = [
        'name.required' => 'Lesson Name is required',
        'subject_id.required' => 'Subject is required',
        'code.unique' => 'Lesson Code must be unique',
    ];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'subject_id' => 'required',
            'code' => 'unique:lessons',
            'name' => 'required|min:3',
            'description' => '',
            'status' => '',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'min:3|unique:roles',
            'guard_name' => 'min:2|in:web,api',
            'code' => 'unique:lessons',
        ]
   ];
}