<?php
namespace App\Validators;

use \Prettus\Validator\LaravelValidator;
use \Prettus\Validator\Contracts\ValidatorInterface;

class CategoryValidator extends LaravelValidator
{
    protected $attributes = [];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'label' => 'required|min:3',
            'description' => '',
            'status' => '',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'min:3',
        ]
   ];
}