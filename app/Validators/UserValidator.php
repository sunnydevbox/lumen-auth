<?php
namespace App\Validators;

use \Prettus\Validator\LaravelValidator;
use \Prettus\Validator\Contracts\ValidatorInterface;

class UserValidator extends LaravelValidator
{
    protected $attributes = [
        'email' => 'E-mail',
        'first_name' => 'First Name',
    ];

    protected $messages = [
        'type.required' => 'Login type is invalid'
    ];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email' => 'required|email|unique:users',
            'first_name' => 'min:2',
            'last_name' => 'min:2',
            'middle_name' => 'min:2',
            'password'  => 'min:8',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email' => 'email|unique:users',
            'password'  => 'min:8',
            'first_name' => 'min:2',
            'last_name' => 'min:2',
            'middle_name' => 'min:2',
            'password'  => 'min:8',
        ],

        'LOGIN' => [
            'email' => 'required',
            'password' => 'required',
            'type'  => 'required',
        ],
   ];
}