<?php

namespace App\Events;

class UserRegisteredEvent extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }
}
