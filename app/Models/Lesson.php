<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Traits\UUids;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Subject;
use App\Models\Category;
use Laravel\Scout\Searchable;

class Lesson extends BaseModel
{
    use UUids, SoftDeletes, Searchable;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'subject_id',
        'name',
        'description',
        'duration',
        'status',
        
        'video_status',
        'video_duration',
        'video_key',
        'video_url',
        'video_size',
        'video_embed',
        'video_created',
        'video_updated',
        'video_player_code',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];




    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'uuid');
    }


    public function categories()
    {
        return $this->belongsToMany(Category::class, 'lesson_categories', 'lesson_id', 'category_id');
    }





    public function getKeyName()
    {
        return 'uuid';
    }
}
