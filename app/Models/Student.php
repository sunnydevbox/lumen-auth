<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\UUids;
use App\Traits\HasAttachment;

class Student extends User
{
    use HasAttachment;

    public $guard_name = 'student';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', // School ID
        'first_name',
        'last_name',
        'middle_name',
        'gender',
        'dob',
        'birth_place',
        'religion',
        'nationality',
        'address_1',
        'address_2',
        'city',
        'state',
        'zipcode',
        'country',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password',
    //     'created_at',
    //     'updated_at',
    //     'deleted_at',
    // ];

    const STATUS_ENROLLED = 'enrolled';
    const STATUS_PENDING = 'pending';
    const STATUS_DROPPED = 'dropped';
    const STATUS_SUSPENDED = 'suspended';

    public function isEnrolled()
    {
        return (self::STATUS_ENROLLED == $this->status) ? true : false;
    }

}
