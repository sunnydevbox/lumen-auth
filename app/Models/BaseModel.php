<?php
namespace App\Models;

use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use LogsActivity;
    protected static $logFillable = true;
    protected static $logOnlyDirty = true;
}