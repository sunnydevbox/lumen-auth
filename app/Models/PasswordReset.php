<?php
namespace App\Models;

use App\Models\BaseModel;

class PasswordReset extends BaseModel
{
    protected $fillable = [
        'email',
        'token',
    ];
}