<?php
namespace App\Models;

use App\Models\BaseModel;
use Kalnoy\Nestedset\NodeTrait;

class Category extends BaseModel
{
    use NodeTrait;
    
    protected $table = 'categories';

    protected $hidden = [
        '_lft',
        '_rgt',
        'updated_at',
        'created_at',
        'deleted_at',
        'pivot',
    ];
}