<?php
namespace App\Models;

use App\Traits\UUids;
use App\Models\User;

class Faculty extends User
{
   public $guard_name = 'faculty';

    protected $table = 'users';
}
