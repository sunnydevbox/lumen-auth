<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use App\Models\BaseModel;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\UUids;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasAttachment;


class User extends BaseModel implements JWTSubject, AuthenticatableContract, AuthorizableContract 
{
    use Authenticatable, Authorizable, UUids, HasRoles, SoftDeletes, HasAttachment;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
        'first_name', 
        'last_name', 
        'middle_name', 
        'dob', 
        'status',
        'dob', 'status', 'gender', 'birth_place', 'religion', 'nationality', 
        'address_1', 'address_2', 'city', 'state', 'zipcode', 'country',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $appends = [
        'avatar',
    ];



    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = trim($value);
    }

    public function setPasswordAttribute($value)
    {
        if(Hash::needsRehash($value) ) {
            $value = Hash::make($value);
        }
        $this->attributes['password'] = $value;
    }


    public function confirmPassword($clearPassword)
    {
        return Hash::check($clearPassword, $this->password);
    }


    public function getAvatarAttribute()
    {
        return $this->attachmentsGroup('avatar')
                    ->keyBy('key')
                    ->map(function($item) {
                        return $item->url;
                    })
                    ->all();
    }




    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getKeyName()
    {
        return 'uuid';
    }
}
