<?php

namespace App\Models;

use App\Models\BaseModel;
use App\Traits\UUids;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentSubject extends BaseModel
{
    use UUids, SoftDeletes;

    public $incrementing = false;

    const STATUS_ENROLLED = 'enrolled';
    const STATUS_DROPPED = 'dropped';
    const STATUS_CANCELLED = 'cancelled';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject_id',
        'enrolled_at',
        'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];


}
