<?php
namespace App\Models;

use App\Models\BaseModel;
use App\Traits\UUids;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends BaseModel
{
    use UUids, SoftDeletes;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'status',
        'code',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function getKeyName()
    {
        return 'uuid';
    }
}
