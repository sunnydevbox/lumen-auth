<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Attachment;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    public function boot()
    {

        // ...

        // Attachment::outputting(function ($attachment) {
        //     /** @var Attachment $attachment */
        //     dd($attachment);
        //     // Get the related model
        //     $model = $attachment->model;

        //     if (empty($model)) {
        //         // Deny output for attachments not linked to a model

        //         return false;
        //     }

        //     if ($model instanceof \App\User) {
        //         // Check if current user is granted and owner

        //         $user = \Auth::user();

        //         return $user && $user->can('download-file') && $user->id == $model->id;
        //     }
        // });

        // ...
    }
}
