<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

class SubjectRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name' => 'like',
        'code' => 'like',
        'description' => 'like',
        'status',
    ];

    public function model()
    {
        return \App\Models\Subject::class;
    }

    public function presenter()
    {
        return \App\Presenters\SubjectPresenter::class;
    }
}