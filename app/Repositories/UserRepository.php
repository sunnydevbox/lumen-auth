<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    function model()
    {
        return \App\Models\User::class;
    }

    public function validator()
    {
        return \App\Validators\UserValidator::class;
    }
}