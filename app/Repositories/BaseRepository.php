<?php
namespace App\Repositories;

use \Prettus\Repository\Eloquent\BaseRepository as PrettusBR;

class BaseRepository extends PrettusBR
{
    public function model() {
        return null;
    }
    
    public function find($id, $columns = ['*'])
    {
        $this->applyCriteria();
        $this->applyScope();
        
        // if (is_string($id)) {
        //     $model = $this->model->find($id);
        // } else {
            $model = $this->model->findOrFail($id, $columns);
        // }
        
        if (!$model) {
            // throw
        }
        
        $this->resetModel();

        return $this->parserResult($model);
    }

    public function boot()
    {
        parent::boot();
        
        $this->pushCriteria(\App\Criteria\RequestCriteria::class);
    }
}