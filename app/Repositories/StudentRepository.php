<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

class StudentRepository extends BaseRepository
{
    function model()
    {
        return \App\Models\Student::class;
    }

    public function validator()
    {
        return \App\Validators\StudentValidator::class;
    }
}