<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;
// use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

class FacultyRepository extends BaseRepository
{
    public function model()
    {
        return \App\Models\Faculty::class;
    }

    public function validator()
    {
        return \App\Validators\FacultyValidator::class;
    }

    public function presenter()
    {
        return \App\Presenters\FacultyPresenter::class;
    }
}