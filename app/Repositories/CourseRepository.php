<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

class CourseRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name' => 'like',
        'code' => 'like',
        'description' => 'like',
        'status',
    ];

    public function model()
    {
        return \App\Models\Course::class;
    }

    // public function presenter()
    // {
    //     return \App\Presenters\RolePresenter::class;
    // }
}