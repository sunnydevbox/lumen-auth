<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

class LessonRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'subject_id' => 'like',
        'name' => 'like',
        'code' => 'like',
        'description' => 'like',
        'status',
    ];

    public function model()
    {
        return \App\Models\Lesson::class;
    }

    public function presenter()
    {
        return \App\Presenters\LessonPresenter::class;
    }
}