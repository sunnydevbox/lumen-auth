<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return \App\Models\Role::class;
    }

    public function presenter()
    {
        return \App\Presenters\RolePresenter::class;
    }
}