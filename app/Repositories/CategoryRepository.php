<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'label' => 'like',
        'description' => 'like',
        'status',
    ];

    public function model()
    {
        return \App\Models\Category::class;
    }

    // public function presenter()
    // {
    //     return \App\Presenters\RolePresenter::class;
    // }
}