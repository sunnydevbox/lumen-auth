<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

class PasswordResetRepository extends BaseRepository
{
    public function model()
    {
        return \App\Models\PasswordReset::class;
    }
}