<?php
namespace App\Transformers;

use App\Models\Lesson;
use League\Fractal\TransformerAbstract;

class LessonTransformer extends TransformerAbstract
{
    public function transform(Lesson $lesson)
    {
        // $u = $user->toArray();
        // $u['roles'] = $user->getRoleNames();
        // $u['permissions'] = $user->getAllPermissions()->pluck('name')->toArray();
        // $u['avatar'] = $user->attachment('avatar');

        return $lesson->toArray();
    }
}