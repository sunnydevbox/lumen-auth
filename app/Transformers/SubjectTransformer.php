<?php
namespace App\Transformers;

use App\Models\Subject;
use League\Fractal\TransformerAbstract;

class SubjectTransformer extends TransformerAbstract
{
    public function transform(Subject $subject)
    {
        // $u = $user->toArray();
        // $u['roles'] = $user->getRoleNames();
        // $u['permissions'] = $user->getAllPermissions()->pluck('name')->toArray();
        // $u['avatar'] = $user->attachment('avatar');

        return $subject->toArray();
    }
}