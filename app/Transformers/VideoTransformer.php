<?php
namespace App\Transformers;

use App\Models\Lesson;
use League\Fractal\TransformerAbstract;
use App\Services\LessonService;

class VideoTransformer extends TransformerAbstract
{
    public function transform(Lesson $lesson)
    {
        $lessonService = app(LessonService::class);
        $data = $lesson->toArray();

        $data['url'] = $lessonService->getVideoURL($lesson->video_key);
        $data['posters'] = $lessonService->getVideoPosterURL($lesson->video_key);

        return $data;
    }
}