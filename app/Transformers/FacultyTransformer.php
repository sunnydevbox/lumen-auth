<?php
namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class FacultyTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $u = $user->toArray();
        $u['roles'] = $user->getRoleNames();
        $u['permissions'] = $user->getAllPermissions()->pluck('name')->toArray();
        $u['avatar'] = $user->attachment('avatar');

        return $u;
        return [
            'id'      => (int) $user->id,
            'name'   => '--' . $user->name,
            // 'content' => $post->content
        ];
    }
}