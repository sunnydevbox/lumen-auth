<?php

if ( ! function_exists('config_path'))
{
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if ( ! function_exists('user'))
{
    /**
     * Get the configuration path.
     *      
     * @param  string $path
     * @return string
     */
    function user($path = '')
    {
        $guards = array_keys(config('auth.guards'));
        
        foreach(config('auth.guards') as $k => $v) {
            if (auth($k)->check()) {            
            // if (app('auth')->guard($k)->check()) {
                return auth($k)->user();
            }
        }

        return null;
    }
}